<?php

  /**
   * File : Billet.php
   *
   * @author G. Canals
   *
   *
   * @package blog
   */

/**
 *  La classe Billet
 *
 *  La Classe Billet  realise un Active Record sur la table Billet
 *  
 *  @package blog
 */
class adherent {

  /**
   *  Identifiant de l'adherent
   *  @access private
   *  @var integer
   */
  private $id; 


  /**
   *  Name de l'adherent
   *  @access private
   *  @var String
   */
  private $Name;

  /**
   *  lastName de l'adherent
   *  @access private
   *  @var String
   */
  private $LastName;

  /**
   * email de l'adherent
   *  @access private
   *  @var String
   */
  private $Mail;

  /**
   * Address de l'adherent
   *  @access private
   *  @var String
   */
  private $Address;

  /**
   * Date de register de l'adherent
   *  @access private
   *  @var String
   */
  private $DateRegister;


  /**
   *  Constructeur de l'adherent
   *
   *  fabrique une nouvelle adherent
   */
  
  public function __construct() {
    // rien à faire
  }


  /**
   *  Magic pour imprimer
   *
   *  Fonction Magic retournant une chaine de caracteres imprimable
   *  pour imprimer facilement un Ouvrage
   *
   *  @return String
   */
  public function __toString() {
        return "[". __CLASS__ . "] id : ". $this->id . ":
				   Name  ". $this->Name  .":
           LastName  ". $this->LastName  .":
           Mail  ". $this->Mail  .":
           Address  ". $this->Address  .":
				   DateRegister ". $this->DateRegister  ;
  }

  /**
   *   Getter generrique
   *
   *   fonction d'acces aux attributs d'un objet.
   *   Recoit en parametre le nom de l'attribut accede
   *   et retourne sa valeur.
   *  
   *   @param String $attr_name attribute name 
   *   @return mixed
   */


  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    throw new Exception($emess, 45);
  }
   

  
  /**
   *   Setter generique
   *
   *   fonction de modification des attributs d'un objet.
   *   Recoit en parametre le nom de l'attribut modifie et la nouvelle valeur
   *  
   *   @param String $attr_name attribute name 
   *   @param mixed $attr_val attribute value
   *   @return mixed new attribute value
   */
    public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) {
      $this->$attr_name=$attr_val; 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    throw new Exception($emess, 45);
    
  }


  /**
   *   Sauvegarde dans la base
   *
   *   Enregistre l'etat de l'objet dans la table
   *   Si l'objet possede un identifiant : mise à jour de l aligne correspondante
   *   sinon : insertion dans une nouvelle ligne
   *
   *   @return int le nombre de lignes touchees
   */
  public function save() {
    if (!isset($this->id)) {
      return $this->insert();
    } else {
      return $this->update();
    }
  }


  /**
   *   mise a jour de la ligne courante
   *   
   *   Sauvegarde l'objet courant dans la base en faisant un update
   *   l'identifiant de l'objet doit exister (insert obligatoire auparavant)
   *   méthode privée - la méthode publique s'appelle save
   *   @acess public
   *   @return int nombre de lignes mises à jour
   */
  public function update() {
    
    if (!isset($this->id)) {
      throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
    } 
    
    $save_query = "UPDATE adherent SET Name=".(isset($this->Name) ? "'$this->Name'" : "null").",
                                        LastName=".(isset($this->LastName) ? "'$this->LastName'" : "null").",
                                        Mail=".(isset($this->Mail) ? "'$this->Mail'" : "null").",
                                        Address=".(isset($this->Address) ? "'$this->Address'" : "null").",
                                        DateRegister=".(isset($this->DateRegister) ? "'$this->DateRegister'" : "null")." where id=$this->id";
    $pdo = Base::getConnection();
    $nb=$pdo->exec($save_query);
    
	return $nb;
    
  }


  /**
   *   Suppression dans la base
   *
   *   Supprime la ligne dans la table corrsepondant à l'objet courant
   *   L'objet doit posséder un OID
   */
  public function delete() {
    
    $pdo = Base::getConnection();

    $sth = $pdo->prepare('DELETE FROM adherent WHERE id = :id');
    
    $nb = $sth->execute(array(':id'=>$this->id));


    return $nb;
  }
		
		
  /**
   *   Insertion dans la base
   *
   *   Insère l'objet comme une nouvelle ligne dans la table
   *   l'objet doit posséder  un code_rayon
   *
   *   @return int nombre de lignes insérées
   */									
  public function insert() {

      $pdo = Base::getConnection();
      
      $sth = $pdo->prepare('INSERT INTO adherent VALUES (null, :Name, :LastName, :Mail, :Address, :DateRegister');      
      $sth->execute(array(':Name'=>$this->Name, ':LastName'=>$this->LastName, ':Mail'=>$this->Mail, ':DateRegister'=>$this->DateRegister));

  }
		

 /**
   *   Finder sur ID
   *
   *   Retrouve la ligne de la table correspondant au ID passé en paramètre,
   *   retourne un objet
   *  
   *   @static
   *   @param integer $id OID to find
   *   @return Adherent renvoie un objet de type Adherent
   */
    public static function findById($id) {
      $query = "SELECT * FROM adherent WHERE id=". " $id ";
      //echo $query;
      $pdo = Base::getConnection();
      $dbres = $pdo->query($query);
      
      $d=$dbres->fetch(PDO::FETCH_OBJ);

      $o = new adherent();
      $o->id = $d->id;
      $o->Name = $d->Name;
      $o->LastName = $d->LastName;
      $o->Mail = $d->Mail;
      $o->Address = $d->Address;
      $o->DateRegister=$d->DateRegister;

      return $o;
    }

}



?>
