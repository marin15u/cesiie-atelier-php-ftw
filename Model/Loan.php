<?php

  class Loan{
	  
	  private $id;
	  private $DepartureD_date;
	  private $Entry_date;
	  private $id_Adh;
	  private $id_Doc;

   public function __construc(){
	   //rien
   }	  
   
   public function __toString(){
	   return "[". __CLASS__."]id:".$this->id.":
	                  Departure_date".$this->Departure_date.":
	                  Entry_date".$this->Entry_date.":
	                  id_Adh".$this->id_Adh.":
	                  id_Doc".$this->id_Doc;
   }
   
   public function __get($attr_name){
	   if(property_exists(__CLASS__,$attr_name)){
		   return $this->$attr_name;
	   }
	   $emess = __CLASS__.":unknown member $att_name (getAttr)";
	   throw new Exception($emess,45);
   }
   
   
   public function __set($att_name,$att_val){
	   if(property_exists(__CLASS__,$attr_name)){
		   $this->$attr_name=$attr_val;
		   return $this->$attr_name;
	   }
	   $emess = __CLASS__.":unknown member $attr_name (setAttr)";
	   throw new Exception($emess,45);
   }
   
    /*INSERT*/
   
   public function insert(){
	   $db=Base::getConnection();  
	   
	   $save_query = "insert into loan (DepartureD_date,Entry_date,id_Adh,id_Doc) values('".$this->__get('Departure_date')."','".$this->__get('Entry_date')."','"$this->__get('id_Adh')."','".$this->__get('id_Doc')."')";
	   
	    try{     

        $aff_rows = $db->exec($save_query);
         $this->__set('id',$db->LastInsertId('loan'));
         echo $this->id;
        return $aff_rows;
    }
    catch(BaseException $e){ 
      throw new PDOException("Error insert".$e->getMessage(). '<br/>');
    }   
   }
   
   /*DELETE*/
   
     public function delete() {
    
    $db=Base::getConnection();   

     if (!isset($this->id)) {
      throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
    } 

   $save_query = "delete from loan where id='".$this->id."'" ;
    try{     
        $exec=$db->prepare($save_query);
        $exec->execute();
        
        return $exec->rowCount();
    }
    catch(BaseException $e){ 
      throw new PDOException("Error Delete".$e->getMessage(). '<br/>');
    }
    
    /*SAVE*/
    
    public function save() {
    if (!isset($this->id)) {
      return $this->insert();
    } else {
      return $this->update();
    }
  }
  
  /*FINDALL*/
   public static function findAll() {

  $query = "select * from loan";
  $pdo=Base::getConnection();
  $dbres=$pdo->query($query);

  $c=$dbres->fetchall(PDO::FETCH_OBJ);

  if(isset($c))
    return $c;
  else
    return false;
  
 }
  
    
  /*FINDBYID*/

   public static function findById($id) {
      $query = "select * from loan where id=".$id ;
      //echo $query;
      $db = Base::getConnection();
      $dbres = $db->query($query);
      
      $c=$dbres->fetch(PDO::FETCH_OBJ) ;

      if(isset($c->id)){
      return $c;  
      }
      else{
        echo "Aucun résultat";
      } 
    }
   
}
   
  }//END

?>