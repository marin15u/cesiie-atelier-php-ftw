<?php

  
class Document {

  private $id;

  private $title;
  
  private $author;

  private $descriptive;

  private $publicationDate;

  private $status;

  private $id_Type;

  private $id_Genre;






  /**
   *  Constructeur de Document
   *
   *  fabrique un document vide
   */
  
  public function __construct() {
    // rien à faire
  }



  /**
   *   Getter generrique
   *
   *   fonction d'acces aux attributs d'un objet.
   *   Recoit en parametre le nom de l'attribut accede
   *   et retourne sa valeur.
   *  
   *   @param String $attr_name attribute name 
   *   @return mixed
   */


  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    throw new Exception($emess, 45);
  }
   

  
  /**
   *   Setter generique
   *
   *   fonction de modification des attributs d'un objet.
   *   Recoit en parametre le nom de l'attribut modifie et la nouvelle valeur
   *  
   *   @param String $attr_name attribute name 
   *   @param mixed $attr_val attribute value
   *   @return mixed new attribute value
   */
    public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) {
      $this->$attr_name=$attr_val; 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    throw new Exception($emess, 45);
    
  }



    public function updateStatusRetour() {
      
      if (!isset($this->id)) {
        throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
      } 


      // on met "Disponible" dans status
      $save_query = "update document set status=".(isset($this->status) ? "'$this->status'" : "null")." where id=$this->id";



      $pdo = Base::getConnection();
      $nb=$pdo->exec($save_query);
      
  	return $nb;
      
    }


    public function updateStatusEmprunt() {
      
      if (!isset($this->id)) {
        throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
      } 


      // on met "Indisponible" dans status
      $save_query = "update document set status=".(isset($this->status) ? "'$this->status'" : "null")." where id=$this->id";



      $pdo = Base::getConnection();
      $nb=$pdo->exec($save_query);
      
    return $nb;
      
    }



    public static function findByCat($id) {

    
      try{
        $query = "SELECT * FROM document where id_Type = " . " $id";
        $pdo = Base::getConnection();
        $dbRes = $pdo->query($query);

        $fAll = $dbRes->fetchAll(PDO::FETCH_OBJ);

        $tabRes = array();

        foreach ($fAll as $c) {
          $o = new Document();
          //$o->id = $c->id;
          $o->title = $c->title;
          $o->descriptive = $c->descriptive;
          $o->publicationDate = $c->publicationDate;
          $o->status = $c->status;
          $o->id_Type = $c->id_Type;
          $o->id_Genre = $c->id_Genre;
          

          $tabRes[] = $o;
        }
      } catch (PDOExecption $e){
        throw new PDOException("Error Processing Request" .$e->getMessage());
      }

        return $tabRes;
    }

   
    
    public static function findAll() {

   
      try{
        $query = "SELECT * FROM document";
        $pdo = Base::getConnection();
        $dbRes = $pdo->query($query);

        $fAll = $dbRes->fetchAll(PDO::FETCH_OBJ);

        $tabRes = array();

        foreach ($fAll as $c) {
          $o = new Document();
          $o->id = $c->id;
          $o->title = $c->Title;
          $o->descriptive = $c->Descriptive;
          $o->author = $c->Author;
          $o->publicationDate = $c->PublicationDate;
          $o->status = $c->Status;
          $o->id_Type = $c->id_Type;
          $o->id_Genre = $c->id_Genre;

          $tabRes[] = $o;
        }
      } catch (PDOExecption $e){
        throw new PDOException("Error Processing Request" .$e->getMessage());
      }

        return $tabRes;
    }

    
    public static function findById($id) {
      $query = "select * from document where id=". " $id ";
      //echo $query;
      $pdo = Base::getConnection();
      $dbres = $pdo->query($query);
      
      $d=$dbres->fetch(PDO::FETCH_OBJ);

      $o = new Document();
      $o->id = $d->id;
      $o->title = $d->Title;
      $o->descriptive = $d->Descriptive;
      $o->author = $d->Author;
      $o->publicationDate = $d->PublicationDate;
      $o->status = $d->Status;
      $o->id_Type = $d->id_Type;
      $o->id_Genre = $d->id_Genre;

      return $o;

    }


    public static function search() {

        $i=0;

        $query = "SELECT * FROM document where ";


        if($_POST['search']){
          $query .= "(title LIKE '%".$_POST['search']."%' OR descriptive LIKE '%".$_POST['search']. "%') ";
          $i++;
        }


        if ($_POST['type']) {
          if($i>0){
            $query .= "AND id_Type = ".$_POST['type']. " ";
          }else{
            $query .= "id_Type = ".$_POST['type']. " ";
          }
          $i++;
        }


        if ($_POST['genre']) {
          if($i>0){
            $query .= "AND id_Genre = ".$_POST['genre']. " ";
          }else{
            $query .= "id_Genre = ".$_POST['genre']. " ";
          }
        }


      echo var_dump($query);


      try{
        
        $pdo = Base::getConnection();
        $dbRes = $pdo->query($query);

        echo var_dump($dbRes);

        $fAll = $dbRes->fetchAll(PDO::FETCH_OBJ);

        $tabRes = array();

        foreach ($fAll as $c) {
          $o = new Document();
          $o->id = $c->id;
          $o->title = $c->Title;
          $o->descriptive = $c->Descriptive;
          $o->author = $c->Author;
          $o->publicationDate = $c->PublicationDate;
          $o->status = $c->Status;
          $o->id_Type = $c->id_Type;
          $o->id_Genre = $c->id_Genre;

          $tabRes[] = $o;
        }
      } catch (PDOExecption $e){
        throw new PDOException("Error Processing Request" .$e->getMessage());
      }

        return $tabRes;
    }




}


?>
