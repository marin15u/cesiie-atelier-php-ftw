<?php


class Vue{


  private $top, $mid, $bot, $document, $genre, $type, $user;

  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    throw new Exception($emess, 45);
  }

  public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) {
      $this->$attr_name=$attr_val; 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    throw new Exception($emess, 45);
    
  }

    public function __construct(){ }

 
    public function afficheGeneral($select){
      switch ( $select ) {
        case 'ListeDocuments':

          $this->top = self::afficheMenu();
          $this->mid = self::afficheListDocuments();
                   
          break;

        case 'Search':


                  
          break;

        case 'DetailDocument':

          $this->top = self::afficheMenu();
          $this->mid = self::afficheDetailsDocuments();
                   
          break;

        case '????':
          
          break;

        
        
        default:
          # code...
          break;
      }


      $msg = "<!DOCTYPE html>
              <html>
                <head>
                    <title>Medianet &bull; Recherche et consultation de documents en ligne</title>
                    <meta charset='UTF-8' />
                    <link rel='stylesheet' href='style/screen.css'>
                    <link rel='icon' type='image/png' href='images/livre.ico' /> <!-- Favicon du site -->
                </head>

            <body>
            <div id='content'>";

      $msg .=   "<nav>"  . $this->top .  "</nav>";
      $msg .=   "<article>"  .$this->mid.  "</article>";

     

      $msg .=   "<footer>

      Copyright GKMC WebDesign

      <p>
        <a href='http://jigsaw.w3.org/css-validator/'>
        <img style='border:0;width:88px;height:31px'
            src='http://jigsaw.w3.org/css-validator/images/vcss'
            alt='CSS Valide !' />
        </a>

        <!-- <a href='http://validator.w3.org/check'>
          <img src='../Images/valid.png' alt='HTML 5 Valide !'>
        </a> !-->
        
      </p>

      </footer>";
      
      echo $msg;

    }


    private function afficheListDocuments(){
    
      $res = "";

      foreach ($this->document as $d) {

        $res .= "<a href='index.php?action=details&id=" .$d->id. "'><section>

                  <h1>".$d->title."</h1>

                  <p> de ".($d->author)."</p>
                  <p>".nl2br($d->descriptive)."</p>" ;

        $res .= "</section></a>" ;
      }
     
      return $res;

    }

    private function afficheMenu(){
  
      $res = "<div class='row'>
                <section class='offset2 span8'>
                  <nav>
                    <form method=POST action='index.php?action=search'>
                      <input type='search' placeholder='Recherche par mot clé' name='search'>
                      <select name='type' id='type'> <option value=''> </option>";
                      foreach ($this->type as $t) {
                          $res .= "<option value=".$t->id.">".$t->type."</option>";
                        }
                        $res .= "</select>
                      <select name='genre' id='genre'> <option value=''> </option>";
                      foreach ($this->genre as $g) {
                        $res .= "<option value=".$g->id.">".$g->genre."</option>";
                        }
                        $res .= "</select>
                      <input type='submit' value='Go !''>
                    </form>
                  </nav>
                </section>
             </div>";

      return $res;
    }

    private function afficheDetailsDocuments(){
    
      $res = "";

        $res .= "<section>

                  <h1>".$this->document->title."</h1>

                  <p> de ".$this->document->author."</p>
                  <p>".nl2br($this->document->descriptive)."</p>
                  <p> ".$this->document->status."</p>
                  <p> ".$this->document->publicationDate."</p>

                  <form action='index.php'> <input type=submit value='Retour Accueil'> </form>" ;

        $res .= "</section>" ;
           
      return $res;

    }













  }